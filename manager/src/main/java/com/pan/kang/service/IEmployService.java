package com.pan.kang.service;

import com.pan.kang.domain.po.EmployPO;
import com.pan.kang.domain.vo.CommonVO;
import com.pan.kang.framework.IContext;
import org.springframework.data.domain.Page;

public interface IEmployService extends IContext<EmployPO, String> {

    /**
     * 根据职员姓名支持模糊查询
     *
     * @param employPO employPO
     * @param limit limit
     * @param offset offset
     * @return
     */
    Page<CommonVO> queryEmployPage(EmployPO employPO, int limit, int offset);
}
