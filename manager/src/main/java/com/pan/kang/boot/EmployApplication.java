package com.pan.kang.boot;


import com.pan.kang.framework.Application;
import com.pan.kang.framework.ApplicationBuilder;


public class EmployApplication extends Application {
    public static void main(final String[] args) throws Exception {
        new ApplicationBuilder(EmployApplication.class).run(args);
    }
}

