package com.pan.kang.repositories;

import com.pan.kang.domain.po.EmployPO;
import com.pan.kang.framework.IGeneralGidCompleteDao;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployRepository extends IGeneralGidCompleteDao<EmployPO, String> {


}
