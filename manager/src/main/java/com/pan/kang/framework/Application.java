package com.pan.kang.framework;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.pan.kang")
@EnableJpaRepositories(basePackages = "com.pan.kang.repositories")
@EntityScan("com.pan.kang.domain.po")
public class Application {
    public static void main(String[] args) throws Exception {
        (new ApplicationBuilder(Application.class)).run(args);
    }
}
