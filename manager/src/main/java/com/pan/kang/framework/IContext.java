package com.pan.kang.framework;

import java.io.Serializable;

public interface IContext<M extends IDataModel, ID extends Serializable> extends IDoMainContext {
    void deleteByKey(ID var1);
    void save(M var1);
    <M> M findById(ID var1);
}
