package com.pan.kang.framework;

import java.io.Serializable;

public interface IGenEntity<ID extends Serializable> extends IIdentifyModel<ID> {
}
