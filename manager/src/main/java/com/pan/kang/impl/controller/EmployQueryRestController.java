package com.pan.kang.impl.controller;

import com.pan.kang.controller.IEmployQueryController;
import com.pan.kang.domain.po.EmployPO;
import com.pan.kang.domain.vo.CommonVO;
import com.pan.kang.framework.GeneralQueryRestService;
import com.pan.kang.service.IEmployService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/query/employ")
public class EmployQueryRestController extends GeneralQueryRestService<EmployPO, String, IEmployService>
        implements IEmployQueryController {

    @PostMapping("/queryEmployPage")
    @ResponseBody
    @Override
    public Page<CommonVO> queryEmployPage(@RequestBody EmployPO employPO,
                                          @RequestParam int limit, @RequestParam int offset) {

        return getContext().queryEmployPage(employPO, limit, offset);
    }
}
