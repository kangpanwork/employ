package com.pan.kang.impl.controller;

import com.pan.kang.controller.IEmployController;
import com.pan.kang.domain.po.EmployPO;
import com.pan.kang.framework.GeneralRestService;
import com.pan.kang.service.IEmployService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/service/employ")
public class EmployController extends GeneralRestService<EmployPO, String, IEmployService>
        implements IEmployController {


}
