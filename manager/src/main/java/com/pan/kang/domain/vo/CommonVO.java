package com.pan.kang.domain.vo;


import com.pan.kang.framework.DynamicValueObject;

public class CommonVO extends DynamicValueObject {

    @Override
    public String toString() {
        return "CommonVO{" +
                "valueMap=" + valueMap +
                '}';
    }
}
