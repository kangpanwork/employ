package com.pan.kang.controller;

import com.pan.kang.domain.po.EmployPO;
import com.pan.kang.domain.vo.CommonVO;
import com.pan.kang.framework.IQueryRestService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface IEmployQueryController extends IQueryRestService {
    Page<CommonVO> queryEmployPage(@RequestBody EmployPO employPO,
                                   @RequestParam int limit, @RequestParam int offset);
}
