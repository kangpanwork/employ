package com.pan.kang.framework;


import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public class IdentifyModel<ID extends Serializable>  implements IIdentifyModel<ID> {
    private static final long serialVersionUID = 1L;

    @Column(name = "GID", length = 36)
    @Id
    private ID gid;

    public IdentifyModel() {
    }


    public ID getGid() {
        return this.gid;
    }

    public void setGid(ID newgid) {
        this.gid = newgid;
    }
}
