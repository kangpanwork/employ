package com.pan.kang.framework;


import org.springframework.boot.builder.SpringApplicationBuilder;

public final class ApplicationBuilder extends SpringApplicationBuilder {
    public ApplicationBuilder(Class<?>... sources) {
        super(sources);
    }
}
