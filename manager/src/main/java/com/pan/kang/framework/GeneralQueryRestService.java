package com.pan.kang.framework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

public abstract class GeneralQueryRestService<Model extends IDataModel, ID extends Serializable,
        CT extends IContext<Model, ID>> implements IRestService {

    @Autowired
    private CT context;
    public GeneralQueryRestService() {
    }

    protected CT getContext() {
        return this.context;
    }

    public void setContext(CT newContext) {
        this.context = newContext;
    }


    @RequestMapping(value = "/findById", method = {RequestMethod.GET})
    @ResponseBody
    public Model find(@RequestParam(value = "gid",required = true) ID gid) {
        return this.getContext().findById(gid);
    }
}
