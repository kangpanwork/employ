package com.pan.kang.framework;

import java.io.Serializable;

public interface IIdentifyModel<ID extends Serializable> extends IDataModel {
    ID getGid();
    void setGid(ID var1);
}
