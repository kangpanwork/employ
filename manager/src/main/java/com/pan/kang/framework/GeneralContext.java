package com.pan.kang.framework;


import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Optional;

public class GeneralContext<M extends IDataModel, ID extends Serializable,
        DAO extends IGeneralGidCompleteDao<M, ID>> implements IContext<M, ID> {

    @Autowired
    private DAO dao;

    protected DAO getDao() {
        return this.dao;
    }


    public GeneralContext() {
    }

    @Override
    public void deleteByKey(ID var1) {
        this.getDao().deleteById(var1);
    }

    @Override
    public void save(M var1) {
        this.getDao().save(var1);
    }

    @Override
    public <M> M findById(ID var1) {
        Optional<M> op = (Optional<M>) this.getDao().findById(var1);
        return op.orElse(null);
    }


}
