package com.pan.kang.domain.po;

import com.pan.kang.framework.GenObjectEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOY")
public class EmployPO extends GenObjectEntity {

    @Column(name = "ENAME", nullable = true, length = 200)
    private String name;

    @Column(name = "AGE", nullable = true, precision = 10)
    private Integer age;

    @Column(name = "BIRTHDAY", nullable = true, length = 200)
    private String birthday;

    @Column(name = "SEX", nullable = true, length = 10)
    private String sex;

    @Column(name = "EMAIL", nullable = true, length = 200)
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
