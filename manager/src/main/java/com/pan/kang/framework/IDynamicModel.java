package com.pan.kang.framework;

import java.io.Serializable;

public interface IDynamicModel extends Serializable {

    <T> T get(String var1);

    <T> void set(String var1, T var2);
}
